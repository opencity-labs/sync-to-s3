# A Docker image to sync folder with a AWS S3 bucket

Use the [AWS-CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) to sync a directory from your filesystem to a S3 bucket.

## Inputs

| Key | Description | Required | Type |
| --- | ----------- | -------- | ---- |
| `AWS_ACCESS_KEY_ID` | The  AWS Access Key | **TRUE** | **SECRET** |
| `AWS_SECRET_ACCESS_KEY` | The AWS secret access key | **TRUE** | **SECRET** |
| `AWS_REGION` | The region of the bucket | **TRUE** | |
| `AWS_BUCKET_NAME` | The bucket to sync | **TRUE** | **SECRET** |
| `SOURCE` | Your local file path that you wish to upload to S3 | **TRUE** | |
| `TARGET` | The destination of the source after sync in S3 | **TRUE** | |
| `WITH_DELETE` | If you want to use the [*--delete* flag](https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html#synopsis) | | | 
